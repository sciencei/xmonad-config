{
  inputs = {
    theme.url = "gitlab:sciencei/nixos-theme";
    # this is only for a nice editing experience
    simformat = {
      url = "github:simspace/simformat";
      flake = false;
    };
  };
  outputs = { self, nixpkgs, simformat, theme }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      # create a shim between nix configuration
      # and haskell-land
      shimPkg = pkgs.linkFarm "xmonadNixShim"
        [ {
            name = "xmonad-nix-shim.cabal";
            path = pkgs.writeText "xmonad-nix-shim.cabal" ''
              name: xmonad-nix-shim
              version: 1.0.0
              build-type: Simple
              cabal-version: >= 1.2

              library
                exposed-modules: NixShim
                build-depends: base
              '';
          }
          {
           name = "NixShim.hs";
            path = pkgs.writeText "NixShim.hs" ''
              module NixShim where
              
              -- * theming
              -- ** colors
              accent  = "${theme.colors.accent}"
              accent2 = "${theme.colors.accent2}"
              accent3 = "${theme.colors.accent3}"
              urgent  = "${theme.colors.bright.red}"
              barFg   = "${theme.colors.foreground}"
              barBg   = "${theme.colors.black}"
              -- ** font
              fontName = "Noto Sans Mono"
              fontSize = "${toString theme.font.size}"

              -- * programs
              terminal = "${pkgs.kitty}/bin/kitty --single-instance"
              browser  = "${pkgs.firefox}/bin/firefox"
              nmcli    = "${pkgs.networkmanager}/bin/nmcli"
              mullvad  = "${pkgs.mullvad-vpn}/bin/mullvad"
              lockCmd  = "/run/wrappers/bin/physlock";
              date     = "${pkgs.coreutils}/bin/date"
              xdotool  = "${pkgs.xdotool}/bin/xdotool"
              -- ** screenshots
              flameshot = "${pkgs.flameshot}/bin/flameshot"
              maim      = "${pkgs.maim}/bin/maim"
              -- ** volume
              pamixer    = "${pkgs.pamixer}/bin/pamixer"

              -- * other
              unicodeFile = "${pkgs.unipicker}/share/unipicker/symbols"
            '';
          }
        ];
      # creating this shim means the rest of our configuration can be plain haskell files, for
      # a better editing experience.
      shimPkgHs = haskellPackages : haskellPackages.callCabal2nix "xmonadNixShimHs" shimPkg {};
      simformatDeriv = pkgs.haskellPackages.callCabal2nix "simformat" simformat {};
      xmonadConfigLib = hsPkgs :
          hsPkgs.callCabal2nix "xmonad-config" ./. { xmonad-nix-shim = (shimPkgHs hsPkgs); };
    in
      {
        config = ./Xmonad.hs;
        deps = haskellPackages: [ (xmonadConfigLib haskellPackages) ];
        devShell."x86_64-linux" = pkgs.haskellPackages.shellFor {
          packages   = p: [ (xmonadConfigLib p) ];
          withHoogle = true;
          # actually tooling
          buildInputs = [
            pkgs.haskellPackages.cabal-install
            pkgs.haskellPackages.ghcid
            pkgs.haskellPackages.haskell-language-server
            simformat
          ];
        };
      };
}
