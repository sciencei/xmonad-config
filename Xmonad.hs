import Graphics.X11.ExtraTypes
  ( xF86XK_AudioLowerVolume, xF86XK_AudioMicMute, xF86XK_AudioMute, xF86XK_AudioRaiseVolume
  )
import XMonad
  ( ChangeLayout(FirstLayout), Full(Full), XConfig(..), (-->), (.|.), (<+>), (|||), KeyMask
  , composeAll, controlMask, mod4Mask, sendMessage, shiftMask, spawn, xK_c, xK_i, xK_m, xK_p, xK_q
  , xK_s, xmonad
  )
import XMonad.Config.Desktop (desktopConfig)
import XMonad.Hooks.FloatNext (floatNextHook)
import XMonad.Hooks.ManageDocks (SetStruts(SetStruts), avoidStruts)
import XMonad.Hooks.ManageHelpers (doFullFloat, isFullscreen)
import XMonad.Hooks.SetWMName ()
import XMonad.Hooks.UrgencyHook (BorderUrgencyHook(BorderUrgencyHook), withUrgencyHook)
import XMonad.Layout.Fullscreen (fullscreenEventHook, fullscreenManageHook)
import XMonad.Layout.NoBorders (noBorders, smartBorders)
import XMonad.Prompt (font)
import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Util.EZConfig (additionalKeys)
import qualified XMonad.Hooks.EwmhDesktops as E
import qualified XMonad.Layout.BinarySpacePartition as BSP

import Bar
import Navigation
import Prompt
import Prompt.Mullvad
import Prompt.NetworkManager
import Prompt.Unicode
import qualified NixShim as Shim

-- windows key
modm :: KeyMask
modm = mod4Mask

main :: IO ()
main = do
  spawnBarFifo
  xmonad
    . E.ewmh
    . with2dNav modm
    . withUrgencyHook (BorderUrgencyHook Shim.urgent)
    $ desktopConfig
        { terminal = Shim.terminal
        , focusedBorderColor = Shim.accent
        , normalBorderColor  = Shim.accent2
        , borderWidth        = 2
        , logHook            = logToBar
        , focusFollowsMouse  = False
        , modMask            = modm
        , handleEventHook    = handleEventHook desktopConfig <+> fullscreenEventHook
        , manageHook = composeAll
            [ floatNextHook
            , fullscreenManageHook
            , isFullscreen --> doFullFloat
            , manageHook desktopConfig
            , ensureBarBelowOtherWindows
            ]
        , layoutHook
            = avoidStruts
            . smartBorders
            $   BSP.emptyBSP
            ||| noBorders Full
        , startupHook = composeAll
            [ sendMessage $ SetStruts [minBound .. maxBound] []
            , sendMessage FirstLayout
            , startupHook desktopConfig
            ]
        }
        `additionalKeys` movementKeys modm
        -- prompts
        `additionalKeys`
        [ ((modm, xK_p), shellPrompt promptConfig)
        , ( (modm .|. shiftMask, xK_p)
          -- TODO: work around lack of font fallback.
          -- probably by manually merging hasklig with noto emoji
          , unicodePrompt (promptConfig)
          )
        , ((modm, xK_m), mullvadPrompt promptConfig)
        , ((modm, xK_i), networkManagerPrompt promptConfig)
        ]
        -- spawning programs
        -- note that there is a default binding of modm + shift + enter to spawn a terminal
        `additionalKeys`
        [ ((modm, xK_c), spawn Shim.browser)
        ]
        `additionalKeys`
        -- screenshots
        [ 
          -- fullscreen
          ( (modm, xK_s)
          , spawn $ Shim.maim <> " ~/shots/full/$(" <> Shim.date <> " +%F-%H-%M-%S.png)"
          )
          -- current window
        , ( (modm .|. shiftMask, xK_s)
          , spawn $ Shim.maim <> " -i $(" <> Shim.xdotool <> " getactivewindow) ~/shots/window/$(" <> Shim.date <> " +%F-%H-%M-%S.png)"
          )
          -- freeform
        , ( (modm .|. controlMask, xK_s)
          , spawn $ Shim.flameshot <> " gui"
          )
        ]
        `additionalKeys`
        -- audio media keys
        -- '0' means no mask
        [ ((0, xF86XK_AudioMute)       , spawn $ Shim.pamixer <> " -t")
        , ((0, xF86XK_AudioLowerVolume), spawn $ Shim.pamixer <> " -d 8")
        , ((0, xF86XK_AudioRaiseVolume), spawn $ Shim.pamixer <> " -i 8")
        , ((0, xF86XK_AudioMicMute)    , spawn $ Shim.pamixer <> " --default-source -t")
        ]
        -- misc
        `additionalKeys`
        [ ((modm .|. controlMask, xK_q), spawn Shim.lockCmd)
        ]
