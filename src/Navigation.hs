-- | Description: Navigation and layout keys
{-# LANGUAGE LambdaCase #-}
module Navigation where

import Prelude

import XMonad
  ( LayoutClass(description), (.|.), KeyMask, X, XConfig, controlMask, sendMessage, shiftMask
  , windows, withWindowSet, xK_h, xK_j, xK_k, xK_l, xK_b, xK_r, xK_n, xK_a, xK_u, KeySym
  )
import qualified XMonad.Actions.Navigation2D as N2
import qualified XMonad.Layout.BinarySpacePartition as BSP
import qualified XMonad.StackSet as W
import XMonad.Hooks.ManageDocks (ToggleStruts(ToggleStruts))

-- 2d navigation only works properly if all windows are mapped
-- it has a hack for mapping unmapped windows that unsurprisingly
-- works like a hack aka not well. instead, we just switch to 1d
-- navigation when we're in "Full" mode, which is the only layout
-- we're currently using which has unmappedwindows
fullNav1D :: (BSP.Direction2D -> t -> X ()) -> BSP.Direction2D -> t -> X ()
fullNav1D f dir wrp = do
  let
    getLayout = withWindowSet (pure . description . W.layout . W.workspace . W.current)
  getLayout >>= \case
    "Full" -> case dir of
      BSP.U -> windows W.focusUp
      BSP.D -> windows W.focusDown
      BSP.L -> windows W.focusUp
      BSP.R -> windows W.focusDown
    _ -> f dir wrp

-- | Add 2D navigation, including moving splits
with2dNav :: KeyMask -> XConfig l -> XConfig l
with2dNav modm =
  let
    navigation = N2.hybridOf N2.lineNavigation N2.centerNavigation
    conf = N2.Navigation2DConfig
      { N2.defaultTiledNavigation = navigation
      , N2.floatNavigation = navigation
      , N2.screenNavigation = navigation
      , N2.layoutNavigation = []
      , N2.unmappedWindowRect = []
      }
  in
    N2.navigation2D conf
      (xK_k, xK_h, xK_j, xK_l)
      [ (modm, fullNav1D N2.windowGo)
      , (modm .|. controlMask, \dir _ -> sendMessage $ BSP.MoveSplit dir)
      , (modm .|. shiftMask, N2.windowSwap)
      ]
      True

movementKeys :: KeyMask -> [((KeyMask, KeySym), X ())]
movementKeys modm = 
  [ ((modm, xK_b), sendMessage ToggleStruts)
  , ((modm .|. shiftMask, xK_b), sendMessage BSP.Swap)
  , ((modm, xK_r), sendMessage BSP.Rotate)
  , ((modm, xK_n), sendMessage BSP.FocusParent)
  , ((modm .|. controlMask, xK_n), sendMessage BSP.SelectNode)
  , ((modm .|. shiftMask, xK_n), sendMessage BSP.MoveNode)
  , ((modm .|. shiftMask, xK_a), sendMessage BSP.Balance)
  , ((modm, xK_a), sendMessage BSP.Equalize)
  , ((modm, xK_u), N2.switchLayer)
  ]
