module Prompt where

import XMonad (controlMask, def, xK_n)
import XMonad.Prompt (XPConfig(..), XPPosition(Top))
import XMonad.Prompt.FuzzyMatch (fuzzyMatch, fuzzySort)
import qualified NixShim as Shim

promptConfig :: XPConfig
promptConfig = def
  { position = Top
  , defaultText = ""
  , promptBorderWidth = 0
  , fgHLight = Shim.barFg
  , bgHLight = Shim.accent
  , fgColor = Shim.barFg
  , bgColor = Shim.barBg
  , completionKey = (controlMask, xK_n)
  , height = 24
  -- TODO: change for unicode
  , font = "xft:" <> Shim.fontName <> "-" <> Shim.fontSize
  , searchPredicate = fuzzyMatch
  , sorter = fuzzySort
  , maxComplRows = Just 5
  }

mkFuzzyComplFunFromList :: [String] -> String -> IO [String]
mkFuzzyComplFunFromList ls s = pure $ filter (\fs -> fuzzyMatch s fs) ls
