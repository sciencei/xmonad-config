module Prompt.NetworkManager where
import XMonad.Prompt (XPrompt(..), getNextCompletion, mkXPrompt)
import qualified NixShim as Shim
import XMonad (X)
import XMonad.Prompt.AppLauncher (XPConfig)
import XMonad.Util.Run (runProcessWithInput, safeSpawn)
import Prompt (mkFuzzyComplFunFromList)

-- | A prompt which allows picking a network connection. Existing connections only, for
-- now
networkManagerPrompt :: XPConfig -> X ()
networkManagerPrompt conf = do
  connections <- lines <$> runProcessWithInput Shim.nmcli ["-t", "-f", "NAME", "c"] ""
  mkXPrompt NetworkManagerPrompt conf (mkFuzzyComplFunFromList connections) $ \str ->
    safeSpawn Shim.nmcli ["c", "up", str]

data NetworkManagerPrompt = NetworkManagerPrompt
instance XPrompt NetworkManagerPrompt where
  showXPrompt NetworkManagerPrompt = "Network: "
  commandToComplete _ = id
  nextCompletion _ = getNextCompletion
