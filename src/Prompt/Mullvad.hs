-- | Description: A prompt for controlling mullvad
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE TypeFamilies #-}
module Prompt.Mullvad where

import Control.Applicative ((<|>))
import Data.Foldable (fold, for_, traverse_)
import Data.Map (Map)
import System.IO (hPutStrLn, stderr)
import qualified Data.Map as Map
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char as M

import XMonad (X, io, spawn)
import XMonad.Prompt (XPConfig, XPrompt, mkXPrompt, showXPrompt, commandToComplete, nextCompletion, getNextCompletion)
import XMonad.Util.Run (runProcessWithInput)

import Prompt (mkFuzzyComplFunFromList)
import qualified NixShim as Shim

mullvadPrompt :: XPConfig -> X ()
mullvadPrompt conf = do
  let
    err = io . hPutStrLn stderr
    mullvadCommands :: [String] -> X ()
    mullvadCommands = traverse_ spawn . fmap ((Shim.mullvad <> " ") <>)
    next :: String -> X ()
    next = \case
      "on" -> mullvadCommands
        [ "always-require-vpn set on"
        , "auto-connect set on"
        , "connect"
        ]
      "off" -> mullvadCommands
        [ "always-require-vpn set off"
        , "auto-connect set off"
        , "disconnect"
        ]
      "reconnect" -> mullvadCommands
        [ "reconnect"
        ]
      "change-region" -> changeRegion
      other -> err $ "invalid input to mullvad prompt: " <> other
    changeRegion = do
      regionsS <- runProcessWithInput Shim.mullvad ["relay", "list"] ""
      let
        regions = M.parseMaybe parseMullvadConnection regionsS
      case regions of
        Nothing -> err "failed to parse mullvad relay list output"
        Just rs -> do
          mkXPrompt (MullvadPrompt ChangeRegion) conf (mkFuzzyComplFunFromList $ Map.keys rs) $ \region ->
            for_ (Map.lookup region rs) $ \regionCode -> spawn $ Shim.mullvad <> " relay set location " <> regionCode
  mkXPrompt (MullvadPrompt Init) conf (mkFuzzyComplFunFromList ["on", "off", "change-region", "reconnect"]) next

-- | parse the output of @mullvad relay list@
parseMullvadConnection :: Parser (Map RegionName RegionCode)
parseMullvadConnection = do
  let
    -- [ and ] are used for beta servers
    word = M.some (M.alphaNumChar <|> M.single ',' <|> M.single '[' <|> M.single ']')
    spaced :: Parser a -> Parser a
    spaced p = p <* M.optional (M.single ' ')
    parens :: Parser a -> Parser a
    parens p = M.between (M.single '(') (M.single ')') p
    restOfLine = M.takeWhileP Nothing (/= '\n') *> M.single '\n' *> pure ()
    region :: Parser (String, String)
    region = do
      -- regions are preceded by a tab
      M.tab
      name <- fmap unwords . M.some $ spaced word
      code <- parens (M.count 3 M.alphaNumChar)
      restOfLine
      -- ignore everything more specific than region, which is preceded by 2 tabs
      M.skipMany (M.try (M.tab *> M.tab) *> restOfLine)
      pure (name, code)
    country :: M.Parsec String String (Map RegionName RegionCode)
    country = do
      countryName <- fmap unwords . M.some $ spaced word
      countryCode <- parens (M.count 2 M.alphaNumChar)
      M.newline
      regions <- M.many region
      M.optional M.newline
      pure $ foldMap (\(regionName, regionCode) -> Map.singleton (regionName <> ", " <> countryName) (countryCode <> " " <> regionCode)) regions
  countryList <- M.many country
  pure $ fold countryList

data MullvadPromptStage
  = Init
  | ChangeRegion

newtype MullvadPrompt = MullvadPrompt MullvadPromptStage
instance XPrompt MullvadPrompt where
  showXPrompt (MullvadPrompt Init) = "Operation: "
  showXPrompt (MullvadPrompt ChangeRegion) = "Region: "
  commandToComplete _ = id
  nextCompletion _ = getNextCompletion

type RegionName = String
type RegionCode = String

type Parser a = M.Parsec String String a
