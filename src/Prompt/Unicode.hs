{-# LANGUAGE LambdaCase #-}
module Prompt.Unicode where

import Codec.Binary.UTF8.String (encodeString)
import Data.Either (fromLeft)
import Data.Function ((&))
import Data.Foldable (foldl')
import GHC.IO.Handle (hClose)

import XMonad (X, io)
import XMonad.Prompt (XPrompt(..), XPConfig, getNextCompletion, mkXPrompt)
import XMonad.Util.Run (spawnPipe, hPutStr)

import Prompt (mkFuzzyComplFunFromList)
import qualified NixShim as Shim

-- | utility to get the head character as a
-- singleton list, else the empty lsit
headOrEmpty :: [a] -> [a]
headOrEmpty [] = []
headOrEmpty (c:_) = [c]

-- | A prompt which allows picking a unicode character by name, which
-- it then types
unicodePrompt :: XPConfig -> X ()
unicodePrompt conf = do
  let
    -- special case where we can't use words
    munge (' ':_) = "  space"
    munge other   = reverse . fromLeft [] $ foldl' go (Right []) other
    -- this file is separated into essentially 3 columns,
    -- but in an annoying way, where the first two are separated
    -- by a single space and the remaining column by a bunch of
    -- spaces. we want to cut out the last column
    -- because it's a bit noisy and things are already slow enough,
    -- so we grab everything up until we find two spaces in a row
    go (Left out) _         = Left out
    go (Right (' ':ls)) ' ' = Left ls
    go (Right ls)       x   = Right (x:ls)

  characters <- fmap munge . lines <$> io (readFile Shim.unicodeFile)
  mkXPrompt UnicodePrompt conf (mkFuzzyComplFunFromList characters) $ \case
    [] -> pure ()
    (c:_) -> do
      io $ do
        handle <- spawnPipe . unwords
                $ Shim.xdotool : ["type", "--clearmodifiers", "--file", "-"]
        hPutStr handle (encodeString [c])
        hClose handle
        pure ()

data UnicodePrompt = UnicodePrompt
instance XPrompt UnicodePrompt where
  showXPrompt UnicodePrompt = "Character: "
  commandToComplete _ = id
  completionToCommand _ = headOrEmpty
  nextCompletion _ s ls = getNextCompletion (headOrEmpty s) (headOrEmpty <$> ls) & \case
    []    -> []
    (c:_) -> [c]
