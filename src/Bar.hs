-- | Description: stuff for populating a top bar, currently polybar
module Bar where

import Prelude

import Codec.Binary.UTF8.String (encodeString)
import Control.Monad (foldM)
import Data.Maybe (catMaybes)

import XMonad
  ( MonadReader(ask), XConf(display), XState(..), (-->), (=?), MonadIO, Query, X, asks, catchX, def
  , gets, idHook, io, liftX, lowerWindow, title
  )
import XMonad.Hooks.DynamicLog (PP(..), pprWindowSet, shorten, wrap)
import XMonad.Hooks.UrgencyHook (readUrgents)
import XMonad.Util.NamedWindows (getName)
import qualified XMonad.StackSet as S

import qualified NixShim as Shim
import XMonad.Util.Run (safeSpawn)

-- | spawn a fifo for the bar to listen on
-- TODO: use nix to ensure that these sync up?
spawnBarFifo :: MonadIO m => m ()
spawnBarFifo = safeSpawn "mkfifo" ["/tmp/.xmonad-bar-log"]

-- | output the log string
logToBar ::  X ()
logToBar = fullDynamicLogString prettyPrinter >>= io . ppOutput prettyPrinter

prettyPrinter :: PP
prettyPrinter = def
  { ppOutput = appendFile "/tmp/.xmonad-bar-log"
  , ppTitle = polyColor Shim.barFg Shim.accent . justifyPad 19 . shorten 15
  , ppSep = " "
  , ppUrgent = polyColor Shim.barFg Shim.urgent . justifyPad 3 . shorten 2
  , ppCurrent = polyColor Shim.barFg Shim.accent . justifyPad 3 . shorten 2
  , ppWsSep = ""
  , ppVisible = polyColor Shim.barFg Shim.accent3 . justifyPad 3 . shorten 2
  , ppHidden = polyColor Shim.barFg Shim.accent2 . justifyPad 3 . shorten 2
  }

ensureBarBelowOtherWindows :: Monoid a => Query a
ensureBarBelowOtherWindows = title =? "polybar-default" --> do
  w <- ask
  liftX $ do
    d <- asks display
    io $ lowerWindow d w
  pure idHook

-- | produce a log string for polybar which has the workspace information
-- adapted from 'dynamicLogString'
fullDynamicLogString :: PP -> X String
fullDynamicLogString pp = do
    winset <- gets windowset
    urgents <- readUrgents
    sorted <- ppSort pp
    -- workspace list
    let
      workspaces = (<> " ") $ pprWindowSet sorted urgents pp winset
      focused    = S.peek winset
      allWindows = reverse $ S.index winset

    windows <- foldM
      (\acc w -> do
        name <- (ppSep pp <>) . (<> ppSep pp) . ppTitleSanitize pp . show <$> getName w
        pure $ if Just w == focused then ppTitle pp name:acc else ppUnfocused name:acc
      )
      []
      allWindows

    -- run extra loggers, ignoring any that generate errors.
    extras <- mapM (`catchX` pure Nothing) $ ppExtras pp


    pure . encodeString . concat . ppOrder pp $
      [ workspaces
      ]
      <> windows
      <> catMaybes extras
      <> ["\n"]
  where
    ppUnfocused = polyColor Shim.barFg Shim.accent2 . justifyPad 19 . shorten 15

-- | Output a string with the given foreground
-- and background colors using polybar/lemonbar
-- escape codes
polyColor
  :: String -- ^ foreground color #rrggbb
  -> String -- ^ background color #rrggbb
  -> String -- ^ string to wrap
  -> String
polyColor fg bg = flip wrap "%{F-}%{B-}" $ "%{F" <> fg  <> "}%{B" <> bg <> "}"

-- | pad a string to the given length with whitespace,
-- centering it
justifyPad :: Int -> String -> String
justifyPad n s
  | length s >= n = s
  | otherwise = replicate left ' ' <> s <> replicate right ' '
  where
    half  = fromIntegral (n - length s) / (2.0 :: Double)
    left  = floor half
    right = ceiling half
